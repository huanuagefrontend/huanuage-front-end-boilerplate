var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  devtool: 'eval',
  entry: [
    'webpack-hot-middleware/client',
    './src/index'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/',
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      inject: true
    })

  ],
  eslint: {
    failOnError: true
  },
  module: {
    preLoaders: [{
      test: /\.js?$/,
      exclude: /node_modules/,
      loaders: ['eslint'],
    }],
    loaders: [{
      test: /\.js$/,
      loader: 'babel',
      include: path.join(__dirname, 'src'),
      exclude: /(node_modules|libs)/,
    }, {
      test: /\.json$/,
      loader: "file-loader",
    }, {
      test: /\.css$/,
      loader: "style!css",
    }, {
      test: /\.(eot|ttf)/,
      loaders: ['file-loader'],
    }, {
      test: /\.(woff|woff2|jpg|png|svg|jpeg|gif)/,
      loaders: ['url-loader?limit=10000'],
    }, {
      test: /\.(woff|woff2)/,
      loaders: ['url-loader?limit=100000'],
    }, {
      test: /vendor\/bootstrap\/dist\/js\/bootstrap\.js$/,
      loader: "imports?$=>./vendor/jquery/dist/jquery.min.js",
    }]
  }
};
