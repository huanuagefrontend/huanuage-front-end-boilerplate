var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  devtool: 'source-map',
  entry: {
    app: './src/index',
    vendors: ['react', 'immutable', 'redux', 'react-router', 'react-redux', 'classnames'],
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
          'NODE_ENV': JSON.stringify('production'),
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
          warnings: false,
      }
    }),
    new webpack.optimize.CommonsChunkPlugin('vendors', 'vendors.js'),
    new ExtractTextPlugin('bundle.css'),
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      hash: true,
      inject: true,
      files: {
          css: 'bundle.css',
      }
    }),
  ],
  module: {
    preLoaders: [{
      test: /\.js?$/,
      loaders: ['eslint'],
    }],
    loaders: [{
      test: /\.js$/,
      loaders: ['babel?stage=0'],
      include: path.join(__dirname, 'src'),
      exclude: /(node_modules|libs)/
    }, {
      test: /\.json$/,
      loaders: ['file-loader']
    }, {
      test: /\.css$/,
      loader: ExtractTextPlugin.extract("style-loader", "css-loader")
    }, {
      test: /\.(eot|ttf)$/,
      loaders: ['file-loader']
    }, {
      test: /\.(woff|woff2|jpg|png|svg|jpeg|gif)/,
      loaders: ['url-loader?limit=10000'],
    }, {
      test: /\.(woff|woff2)/,
      loaders: ['url-loader?limit=100000']
    }]
  }
};
