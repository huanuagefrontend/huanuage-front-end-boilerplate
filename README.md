### 環境要求
node.js
git

### 開發

```
npm install
npm start
open http://localhost:3000
```

### Linting

```
npm run lint
```

###發佈

```
npm run deploy
```
編議後檔案放置於dist資料夾