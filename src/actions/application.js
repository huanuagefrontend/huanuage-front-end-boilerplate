export const INIT_APPLICATION_REQUEST = 'INIT_APPLICATION_REQUEST';
export const INIT_APPLICATION_SUCCESS = 'INIT_APPLICATION_SUCCESS';
export const INIT_APPLICATION_FAIL = 'INIT_APPLICATION_FAIL';
export function initApplication() {
  return (dispatch) => {
    dispatch({
      type: INIT_APPLICATION_REQUEST,
    });

    setTimeout(() => {
      dispatch({
        type: INIT_APPLICATION_SUCCESS,
      });
    }, 1000);
  };
}

export const SWITCH_LOCALE = 'SWITCH_LOCALE';
export function switchLocale(locale) {
  return {
    type: SWITCH_LOCALE,
    locale: locale,
  };
}
