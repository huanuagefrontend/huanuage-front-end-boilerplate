import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './containers/App';
import HomePage from './containers/pages/HomePage';
import { createHistory, useBasename } from 'history';
import configureStore from './store/configureStore';
import {initApplication} from './actions/application';
import {addLocaleData} from 'react-intl';
import en from 'react-intl/lib/locale-data/en';
import zh from 'react-intl/lib/locale-data/zh';


addLocaleData(en);
addLocaleData(zh);

export const store = configureStore({
  application: {
    isLoading: true,
    locale: 'en',
  },
});
store.dispatch(initApplication());
export const history = useBasename(createHistory)({
  basename: '/',
});

export default (
  <Route path="/" component={App} >
    <Route path="/Home" component={HomePage} />
    <IndexRoute component={HomePage} />
  </Route>
);
