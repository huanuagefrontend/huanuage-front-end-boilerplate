import 'isomorphic-fetch';
import { camelizeKeys, pascalizeKeys} from 'humps';
import config from '../config.js';
import queryString from 'query-string';

export const CALL_API = Symbol('Call API');

const API_ROOT = config.API_SERVER;

export const CONTENT_TYPE = {
  JSON: Symbol('json'),
  FORM: Symbol('form'),
};

function callApi(url, params, method = 'GET', type = CONTENT_TYPE.JSON) {
  let fullUrl = (url.indexOf(API_ROOT) === -1) ? API_ROOT + url : url;
  const fetchOptions = {
    mode: 'cors',
    credentials: 'include',
    method: method,
  };

  switch (type) {
  case CONTENT_TYPE.JSON:
    fetchOptions.headers = {'Content-Type': 'application/json'};
    if (typeof(params) !== 'undefined' && method === 'GET' ) {
      fullUrl += '?' + queryString.stringify(params);
    } else if (typeof params !== 'undefined') {
      fetchOptions.body = JSON.stringify(pascalizeKeys(params));
    }
    break;
  case CONTENT_TYPE.FORM:
    if (typeof(params) !== 'undefined') {
      fetchOptions.body = params;
    }
    break;
  default:
    break;
  }
  return fetch(fullUrl, fetchOptions)
  .then( response =>{
    return response.json().then(json => ({json, response}));
  })
  .then(({json, response}) => {
    const camelizedJson = camelizeKeys(json);
    if (typeof(camelizedJson.error) !== 'undefined') {
      return Promise.reject({
        'status': camelizedJson.error.status,
        'message': camelizedJson.error.detail,
      });
    }

    if (!response.ok) {
      return Promise.reject({
        'status': response.status,
        'message': response.statusText,
      });
    }
    return camelizedJson;
  });
}

export default () => next => action => {
  const callAPI = action[CALL_API];
  if (typeof(callAPI) === 'undefined') {
    return next(action);
  }

  function actionWith(data) {
    const finalAction = Object.assign({}, action, data);
    delete finalAction[CALL_API];
    return finalAction;
  }

  const {types, params, method, contentType, url, onSuccess, onFail} = callAPI;

  const [requestType, successType, failureType] = types;
  next({
    type: requestType,
  });

  return callApi(url, params, method, contentType).then(
      response => {
        next(actionWith({
          response,
          type: successType,
        }));

        if (typeof onSuccess === 'function') {
          onSuccess();
        }

        return Promise.resolve(response);
      },
      (error) =>{
        next(actionWith({
          type: failureType,
          error: error,
        }));

        if (typeof onFail === 'function') {
          onSuccess();
        }

        return Promise.reject();
      }
    );
};
