import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import rootReducer from '../reducers/';
import api from '../middlewares/api';
import {Iterable} from 'immutable';
let createStoreWithMiddleware = createStore;

if (process.env.NODE_ENV === 'production') {
  createStoreWithMiddleware = applyMiddleware(
    api,
    thunkMiddleware
  )(createStore);
} else {
  const loggerMiddleware = createLogger({
    transformer: (state) => {
      const newState = {};
      for (const key of Object.keys(state)) {
        if (Iterable.isIterable(state[key])) {
          newState[key] = state[key].toJS();
        } else {
          newState[key] = state[key];
        }
      }
      return newState;
    },
  });

  createStoreWithMiddleware = applyMiddleware(
    api,
    thunkMiddleware,
    loggerMiddleware
  )(createStore);
}

export default function configureStore(initialState) {
  return createStoreWithMiddleware(rootReducer, initialState);
}
