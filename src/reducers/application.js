import {
  INIT_APPLICATION_REQUEST,
  INIT_APPLICATION_SUCCESS,
  SWITCH_LOCALE,
} from '../actions/application';
export default function application(state = {
  isLoading: false,
  locale: 'en',
}, action) {
  switch (action.type) {
  case INIT_APPLICATION_REQUEST:
    return Object.assign({}, state, {isLoading: true});
  case INIT_APPLICATION_SUCCESS:
    return Object.assign({}, state, {isLoading: false});
  case SWITCH_LOCALE:
    return Object.assign({}, state, {locale: action.locale});
  default:
    return state;
  }
}
