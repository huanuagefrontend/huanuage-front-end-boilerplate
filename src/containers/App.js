import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import logo from '../images/logo_s_w.png';
import {IntlProvider} from 'react-intl';
import {
  switchLocale,
} from '../actions/application';
import * as i18n from '../i18n/';

class App extends Component {
  constructor() {
    super();
    this.handleSwitchLocal = this.handleSwitchLocal.bind(this);
  }

  handleSwitchLocal(event) {
    this.props.switchLocale(event.target.value);
  }

  render() {
    const {isLoading, locale} = this.props.application;
    const messages = i18n[locale.replace('-', '')];

    return (
      <IntlProvider locale={locale} messages={messages}>
        <div>
          <h1 className="logo">
            <img src={logo} />
            <select style={{
              fontSize: 14,
              outline: 'none',
              borderRadius: 5,
              padding: 3,
              float: 'right',
            }}
              value={locale}
              onChange={this.handleSwitchLocal}
            >
              <option value="zh-Hant">繁體中文</option>
              <option value="en">English</option>
            </select>
          </h1>
          <div>
          {
            isLoading ? 'Loading' : this.props.children
          }
          </div>
        </div>
      </IntlProvider>
    );
  }
}

App.propTypes = {
  application: PropTypes.shape({
    isLoading: PropTypes.bool.isRequired,
    locale: PropTypes.string.isRequired,
  }).isRequired,
  switchLocale: PropTypes.func.isRequired,
  children: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    application: state.application,
  };
}


export default connect(mapStateToProps, {switchLocale})(App);
