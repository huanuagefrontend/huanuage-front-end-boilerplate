import React, {Component} from 'react';
import {FormattedMessage} from 'react-intl';

export default class HomePage extends Component {
  render() {
    return (
      <div><FormattedMessage id="home.helloworld" /></div>
    );
  }
}
