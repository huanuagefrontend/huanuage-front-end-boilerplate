import 'babel-core/polyfill';
import React from 'react';
import {Provider} from 'react-redux';
import {Router} from 'react-router';
import ReactDOM from 'react-dom';
import Routes, {store, history} from './routes';
import 'normalize-css';
import './stylesheets/layout.css';

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      {Routes}
    </Router>
  </Provider>
, document.getElementById('root'));
