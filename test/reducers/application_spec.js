import {expect} from 'chai';
import reducer from '../../src/reducers/application';
import * as types from '../../src/actions/application';

describe('Reducer::Application', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {})).to.deep.equal({
      isLoading: false,
    });
  });

  it('should handle INIT_APPLICATION_REQUEST', () => {
    const state = {
      isLoading: false,
    };

    const newState = reducer(state, {type: types.INIT_APPLICATION_REQUEST});
    expect(newState).to.deep.equal({isLoading: true});
  });

  it('should handle INIT_APPLICATION_SUCCESS', () => {
    const state = {
      isLoading: false,
    };

    const newState = reducer(state, {type: types.INIT_APPLICATION_SUCCESS});
    expect(newState).to.deep.equal({isLoading: false});
  });
});
