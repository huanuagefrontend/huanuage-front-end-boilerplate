import {expect} from 'chai';
import {applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import reducer from '../../src/reducers/application';
import * as types from '../../src/actions/application';
import * as actions from '../../src/actions/application';

const middlewares = [thunk];

function mockStore(getState, expectedActions, done) {

  function mockStoreWithoutMiddleware(){
    return {
      getState() {
        return typeof getState === 'function'? getState() : getState;
      },
      dispatch(action) {
        const expectedAction = expectedActions.shift();
        try {
          expect(action).to.deep.equal(expectedAction);

          if( done && !expectedActions.length) {
            done()
          }
          return action;
        } catch (e) {
          done(e);
        }
      }

    }
  }

  const mockStoreWithMiddleware = applyMiddleware(
    ...middlewares
  )(mockStoreWithoutMiddleware);

  return mockStoreWithMiddleware();
}


describe('Action::Application', () => {
  it('creates INIT_APPLICATION_SUCCESS when application has been inited', (done) => {
    const expectedActions = [
      {
        type: types.INIT_APPLICATION_REQUEST,
      },
      {
        type: types.INIT_APPLICATION_SUCCESS,
      },
    ];
    const store = mockStore({isLoading: false}, expectedActions, done);
    store.dispatch(actions.initApplication());
  });
});
