var path = require('path');
var express = require('express');
var webpack = require('webpack');
var config = require('./webpack.dev.config');
var history = require('connect-history-api-fallback');
var app = express();
var compiler = webpack(config);

app.use(history({
  index: '/static/index.html',
  verbose: true,
  logger: console.log.bind(console),
}));

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
}))

app.use(require('webpack-hot-middleware')(compiler));

app.listen(3000, 'localhost', function(err){
    if(err){
        console.log(err);
        return;
    }

    console.log('Listening at http://localhost:3001');
});